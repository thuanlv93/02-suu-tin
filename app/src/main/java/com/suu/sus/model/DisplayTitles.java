package com.suu.sus.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ThuanLV on 11/1/2016.
 */

public class DisplayTitles {

    @SerializedName("title")
    private String title;

    @SerializedName("subtitle")
    private String subtitle;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
}
