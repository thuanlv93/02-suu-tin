package com.suu.sus.model;

/**
 * Created by ThuanLV on 10/31/2016.
 */

public class DataBind {
    private String title;
    private String content;
    private String imagePath;

    public DataBind(String title, String content, String imagePath) {
        this.title = title;
        this.content = content;
        this.imagePath = imagePath;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getImagePath() {
        return imagePath;
    }
}
