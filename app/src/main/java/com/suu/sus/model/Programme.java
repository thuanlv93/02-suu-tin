package com.suu.sus.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ThuanLV on 11/1/2016.
 */

public class Programme {
    @SerializedName("title")
    private String title;

    @SerializedName("short_synopsis")
    private String shortSynopsis;

    @SerializedName("image")
    private Image image;

    @SerializedName("display_titles")
    private DisplayTitles displayTitles;

    public String getTitle() {
        return title;
    }

    public String getShortSynopsis() {
        return shortSynopsis;
    }

    public Image getImage() {
        return image;
    }

    public DisplayTitles getDisplayTitles() {
        return displayTitles;
    }
}
