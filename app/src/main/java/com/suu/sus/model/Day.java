package com.suu.sus.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ThuanLV on 11/1/2016.
 */

public class Day {
    @SerializedName("broadcasts")
    private ArrayList<Broadcasts> broadcasts;

    public ArrayList<Broadcasts> getBroadcasts() {
        return broadcasts;
    }

    public void setBroadcasts(ArrayList<Broadcasts> broadcasts) {
        this.broadcasts = broadcasts;
    }
}
