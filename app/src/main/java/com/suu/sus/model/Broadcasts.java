package com.suu.sus.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ThuanLV on 11/1/2016.
 */

public class Broadcasts {

    @SerializedName("programme")
    private Programme programme;

    public Programme getProgramme() {
        return programme;
    }

    public void setProgramme(Programme programme) {
        this.programme = programme;
    }
}
