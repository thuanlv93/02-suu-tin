package com.suu.sus.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ThuanLV on 11/1/2016.
 */

public class Image {

    @SerializedName("pid")
    private String pid;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}
