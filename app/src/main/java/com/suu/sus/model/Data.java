package com.suu.sus.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ThuanLV on 11/1/2016.
 */

public class Data {
    @SerializedName("schedule")
    private Schedule schedule;

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }
}
