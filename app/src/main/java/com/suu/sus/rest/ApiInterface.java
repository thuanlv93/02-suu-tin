package com.suu.sus.rest;

import com.suu.sus.model.Data;

import retrofit2.Call;
import retrofit2.http.GET;


public interface ApiInterface {
    @GET("fm/today.json")
    Call<Data> getSchedule();

//    @GET("movie/{id}")
//    Call<Weather> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);
}
