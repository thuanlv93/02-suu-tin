package com.suu.sus.ui;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.suu.sus.R;
import com.suu.sus.databinding.ActivityMainBinding;
import com.suu.sus.model.DataBind;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    private ActivityMainBinding mBinding;
    private MainContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        mPresenter = new MainPresenter(this);
        mPresenter.getDataRequest();

        mBinding.recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);

        MobileAds.initialize(this, getString(R.string.banner_ad_unit_id));
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mBinding.adView.loadAd(adRequest);
    }

    /**
     * Called when leaving the activity
     */
    @Override
    public void onPause() {
        if (mBinding.adView != null) {
            mBinding.adView.pause();
        }
        super.onPause();
    }

    /**
     * Called when returning to the activity
     */
    @Override
    public void onResume() {
        super.onResume();
        if (mBinding.adView != null) {
            mBinding.adView.resume();
        }
    }

    /**
     * Called before the activity is destroyed
     */
    @Override
    public void onDestroy() {
        if (mBinding.adView != null) {
            mBinding.adView.destroy();
        }
        super.onDestroy();
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void loadData(ArrayList<DataBind> dataBindArrayList) {
        MyAdapter mAdapter = new MyAdapter(this, dataBindArrayList);
        mBinding.recyclerView.setAdapter(mAdapter);
    }
}
