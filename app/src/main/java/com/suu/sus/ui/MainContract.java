package com.suu.sus.ui;

import android.content.Context;

import com.suu.sus.model.DataBind;

import java.util.ArrayList;

/**
 * Created by ThuanLV on 10/28/2016.
 */

public interface MainContract {

    interface View {
        Context getActivityContext();

        void loadData(ArrayList<DataBind> dataBindArrayList);
    }

    interface Presenter {
        void getDataRequest();
    }
}
