package com.suu.sus.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.suu.sus.R;
import com.suu.sus.model.DataBind;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    public ArrayList<DataBind> dataBindArrayList;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView title;
        private TextView content;
        private ImageView imageView;

        ViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.titleTextView);
            content = (TextView) v.findViewById(R.id.contentTextView);
            imageView = (ImageView) v.findViewById(R.id.imageView);
        }
    }

    private Context mContext;

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(Context context, ArrayList<DataBind> dataBindArrayList) {
        mContext = context;
        this.dataBindArrayList = dataBindArrayList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_data, parent, false);
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.title.setText(dataBindArrayList.get(position).getTitle());
        holder.content.setText(dataBindArrayList.get(position).getContent());
        Picasso.with(mContext)
                .load(String.format("http://ichef.bbci.co.uk/images/ic/160x160/%s.jpg", dataBindArrayList.get(position).getImagePath()))
                .fit()
                .centerCrop()
                .into(holder.imageView);
//
//        holder.title.setText("Title " + position);
//        holder.content.setText("Content " + position);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dataBindArrayList.size();
    }
}