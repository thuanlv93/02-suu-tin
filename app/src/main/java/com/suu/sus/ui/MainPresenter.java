package com.suu.sus.ui;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.suu.sus.model.Broadcasts;
import com.suu.sus.model.Data;
import com.suu.sus.model.DataBind;
import com.suu.sus.rest.ApiClient;
import com.suu.sus.rest.ApiInterface;
import com.suu.sus.utils.PreferencesUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ThuanLV on 10/28/2016.
 */

public class MainPresenter implements MainContract.Presenter {

    private MainContract.View mView;
    private final static String APP_ID = "6007c036e3d29e9affc8af842cd01472";


    public MainPresenter(MainContract.View view) {
        mView = view;
    }

    private String getCache() {
        return PreferencesUtils.getString(mView.getActivityContext(), "cache", "");
    }

    private void saveCache(String json) {
        PreferencesUtils.putString(mView.getActivityContext(), "cache", json);
    }

    private void loadData(Data data) {
        try {
            if (data != null) {
                Gson gson = new Gson();
                saveCache(gson.toJson(data));
            } else if (!TextUtils.isEmpty(getCache())) {
                Gson gson = new Gson();
                data = gson.fromJson(getCache(), Data.class);
            }
            if (data != null) {
                ArrayList<DataBind> dataBindArrayList = new ArrayList<>();
                for (Broadcasts broadcasts : data.getSchedule().getDay().getBroadcasts()) {
                    dataBindArrayList.add(new DataBind(
                            broadcasts.getProgramme().getDisplayTitles().getTitle(),
                            broadcasts.getProgramme().getShortSynopsis(),
                            broadcasts.getProgramme().getImage() != null ? broadcasts.getProgramme().getImage().getPid() : ""));

                }
                mView.loadData(dataBindArrayList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getDataRequest() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<Data> data = apiService.getSchedule();
        data.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                if (response.isSuccessful()) {
                    loadData(response.body());
                }
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {

            }
        });
    }
}
